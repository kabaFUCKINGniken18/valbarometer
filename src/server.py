import cherrypy


class StringGenerator(object):
    @cherrypy.expose
    def index(self):
        return open('index.html')



@cherrypy.expose
class StringGeneratorWebService(object):


    def __init__(self,var_queue=None):
        self.var_queue = var_queue

    @cherrypy.tools.accept(media='text/plain')
    def GET(self):
        return cherrypy.session['mystring']

    def POST(self, bar=0):
        #print "post:"
        print "try"
        print bar
        try:
            int(bar)
            self.increase_bar(int(bar))
        except:
            print "wrong value of bar:"
            print bar

        # some_string = ''.join(random.sample(string.hexdigits, int(bar)))
        # cherrypy.session['mystring'] = some_string
        # return some_string
        return ""

    def PUT(self, another_string):
        cherrypy.session['mystring'] = another_string

    def DELETE(self):
        cherrypy.session.pop('mystring', None)


    def increase_bar(self, bar_value=-1):
        if self.var_queue is not None:
            self.var_queue.put(bar_value)

