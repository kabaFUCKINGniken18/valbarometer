from Tkinter import *
import os
import PIL
from PIL import Image
import PIL.ImageTk
import Queue


class Valbarometer(object):

    def __init__(self, master=None, var_queue=None, img_and_barcolor = None, title="Valbarometer"):
        self.height = master.height
        #print self.height
        self.width = master.width
        #print self.width
        self.master = master
        self.title = title
        self.var_queue = var_queue
        self.bar_list = []
        self.count_list = []
        self.label_list = []
        self.img_list = []
        self.img_label_list = []
        if img_and_barcolor is None:
            self.img_and_barcolor = [("img/rodval.png", 'red'),("img/graval.png", 'grey'),
                                     ("img/blaval.png", 'blue'),("img/vitval.png",'white'),("img/valman.png", 'black')                                 ]
        else:
            self.img_and_barcolor = img_and_barcolor

        self.number_of_bars = len(self.img_and_barcolor)
        self.background = self.master.background
        self.increase_size = (self.height - 400)/30
        self.graph_width = self.width / self.number_of_bars
        self.next_graph_start_pos = self.graph_width / 4
        self.next_graph_stop_pos = self.graph_width * 3 / 4


    def increase_bar(self,bar_nbr):
        if 0 < bar_nbr and bar_nbr <= len(self.bar_list):
            self.inc(self.bar_list[bar_nbr - 1])
            temp = self.count_list[bar_nbr - 1].get()
            self.count_list[bar_nbr - 1].set(temp + 1)
        elif bar_nbr == 0:
            self.clear_count()
            for bar in self.count_list:
                bar.set(0)



    def create_image(self,bars):
        """Function to create a list of images from a list of src paths"""
        current_path = os.path.dirname(os.path.abspath(__file__))
        i = 0
        for bar in bars:
            img_src = bar[0]
            src_img_path = os.path.join(current_path,img_src)
            if self.graph_width < 500:
                newsize = (self.graph_width + self.graph_width/2, self.graph_width + self.graph_width/2)
            else:
                newsize = (500,500)
            img = Image.open(src_img_path)
            img.thumbnail(newsize)
            new_file_name="bar_img"+str(i) + ".png"
            img.save(new_file_name)
            img_path = os.path.join(current_path, new_file_name)
            #self.img_list.append(img)
            self.img_list.append(PIL.ImageTk.PhotoImage(file=img_path))
            print img_path
            i+=1


    def draw_frame(self):
        self.frame = Frame(self.master.frame)
        self.frame.configure(background=self.background)
        self.top_frame = Frame(self.frame)
        self.bottom_frame = Frame(self.frame)
        self.bottom_frame.configure(background=self.background)
        Label(self.frame,text=self.title,bg=self.background,font=("Helvetica", 40)).pack()
        self.top_frame.pack(fill=X)
        self.canvas = Canvas(self.frame, background=self.background, width=self.width, height=self.height - 400)
        self.canvas.pack()
        self.bottom_frame.pack(fill=X)
        self.create_image(self.img_and_barcolor)
        self.create_bars()
        return self.frame

    def create_bars(self):
        """function to create the bars, counters and images for each entity"""
        for i in xrange(self.number_of_bars):
            bar = self.canvas.create_rectangle(self.next_graph_start_pos, self.height - 400, self.next_graph_stop_pos, self.height - 400,
                                               fill=self.img_and_barcolor[i][1])
            self.bar_list.append(bar)
            self.next_graph_start_pos += self.graph_width
            self.next_graph_stop_pos += self.graph_width

            # bar counters

            self.top_frame.grid_columnconfigure(i, weight=1)
            intvar = IntVar()
            label = Label(master=self.top_frame, textvariable=intvar, font=("Helvetica", 40))
            label.grid(row=1, column=i)
            self.label_list.append(label)
            self.count_list.append(intvar)
            self.bottom_frame.grid_columnconfigure(i, weight=1)
            img_label = Label(master=self.bottom_frame,image=self.img_list[i],background=self.background)
            img_label.grid(row=0,column=i,sticky=N+W+E)
            self.img_label_list.append(img_label)

        self.master.root.bind("<Key>", self.key)

    def shacke_img(self,nbr,steps):
        if steps == 3:
            self.img_label_list[nbr].grid(row=0,column=nbr,sticky=N+W)
        if steps == 2:
            self.img_label_list[nbr].grid(row=0, column=nbr, sticky=N+E + W)
        if steps == 1:
            self.img_label_list[nbr].grid(row=0,column=nbr,sticky=N+E)
        if steps < 1:
            self.img_label_list[nbr].grid(row=0, column=nbr, sticky=N + E + W)
        else:
            self.master.root.after(100,self.shacke_img,nbr,(steps-1))


    def inc(self,column):
        x0, y0, x1, y1 = self.canvas.coords(column)
        y0=y0-self.increase_size
        self.canvas.coords(column, x0, y0, x1, y1)
        self.shacke_img(column-1,3)

    def clear_count(self):
        for bar in self.bar_list:
            x0, y0, x1, y1 = self.canvas.coords(bar)
            y0 = self.height - 400
            self.canvas.coords(bar, x0, y0, x1, y1)

    def key(self,event):
        char = event.char
        print char
        for i in xrange(len(self.bar_list)):
            if char == chr(i + ord('1')):
                self.inc(self.bar_list[i])
                temp = self.count_list[i].get()
                self.count_list[i].set(temp+1)

        if char=='0':
            self.clear_count()
            for i in self.count_list:
                i.set(0)





