import threading

import Tkinter as tk
from meny import Menu

class TkApp(threading.Thread):
    def __init__(self,fullscreen_on=False, var_queue=None):
        self.fullscreen_on = fullscreen_on
        self.var_queue = var_queue

        threading.Thread.__init__(self)

    def run(self):
        self.root = tk.Tk()
        self.root.configure(background='black')
        self.root.title("IQ Wave ")
        if self.fullscreen_on:
            self.root.attributes('-fullscreen', True)
            self.root.geometry("{0}x{1}+0+0".format(self.root.winfo_screenwidth(),
                                                    self.root.winfo_screenheight()))

        self.width = self.root.winfo_screenwidth()
        self.height = self.root.winfo_screenheight()
        app = Menu(master=self.root, width=self.width, height=self.height, var_queue=self.var_queue)

        self.root.mainloop()

    def stop_mainloop(self):
        self.root.destroy()

