

from Tkinter import *
from valbarometer import Valbarometer
import os
import PIL
import socket
from PIL import Image
import PIL.ImageTk
import Queue
from own_omx_wrapper import OwnOmxWrapper


class Menu(object):

    def __init__(self, master=None, width=None, height=None, var_queue=None):
        self.var_queue = var_queue
        self.root = master
        self.width = width
        self.height = height
        # self.player = omxwrapper(['-o', 'hdmi', '-r'])
        # self.player = omxwrapper(['-o', 'both', '--no-osd','-z'])
        self.player = OwnOmxWrapper(['-o', 'both', '--no-osd'])

        self.background = 'whitesmoke'
        self.frame = Frame(self.root)
        self.frame.place(relx=0, rely=0, relwidth=1, relheight=1)
        self.frame.columnconfigure(0,weight=1)
        self.frame.rowconfigure(0,weight=1)
        self.frame.configure(background='black')
        self.host_label = None
        self.val = Valbarometer(master=self, var_queue=self.var_queue)
        self.loading_label = Label(self.frame,text="Loading...",font=("Helvetica", 40))
        self.loading_label.grid(row=0,column=0,sticky=NSEW)
        self.kabaren_pic_frame = self.draw_kabaren_pic()
        self.root.after(200,self.que_reader)
        self.root.after(100,self.set_host_name_label)

    def set_host_name_label(self):
        self.val_frame = self.val.draw_frame()
        self.loading_label.destroy()
        self.host_label = Label(self.frame, text="ERROR. No network. \n Connect and powercycle", font=("Helvetica", 40))
        self.host_label.grid(row=0,column=0,sticky=NSEW)
        self.host_ip, self.host_name = self.get_Host_name_IP()
        self.host_label.destroy()
        self.host_label = Label(self.frame,
                                text="Hostname :  {}:8080 \n host ip: {}:8080".format(self.host_name, self.host_ip),
                                font=("Helvetica", 40))
        self.host_label.grid(row=0, column=0, sticky=NSEW)

    def get_Host_name_IP(self):
        try:
            host_name = socket.gethostname()
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(('10.255.255.255', 1))
            host_ip = s.getsockname()[0]
            print("Hostname :  ", host_name)
            print("IP : ", host_ip)
            return host_ip,host_name
        except:
            print("Unable to get Hostname and IP")



    def que_reader(self):
        try:
            while not self.var_queue.empty():
                i = self.var_queue.get(0)
                if(i<10):
                    """increses the bars 1-9  and clears at 0"""
                    print "got:"
                    print i
                    self.val.increase_bar(i)
                if(i==20):
                    """do a blackout"""
                    try:
                        self.player.stop()
                    except:
                        print "pause error"
                    self.clear_frames_grid()

                if(i==21):
                    """show the kabare picture"""
                    self.clear_frames_grid()
                    self.kabaren_pic_frame.grid(row=0,column=0,sticky=NSEW)

                if(i == 40):
                    """show some sort of intro to valdebatt"""
                    self.show_bars_intro()

                if (i == 50):
                    """show valdebatt"""
                    self.clear_frames_grid()
                    self.val_frame.grid(row=0, column=0, sticky=NSEW)

                if (i == 101):
                    """stop movie"""

                    self.player.stop()


                if (i == 102):
                    """pause play movie"""

                    self.player.pause()


                if (i == 110):
                    """show the intro movie"""
                    #self.show_intro_mov()
                    self.player.play("sample.mp4")
                    self.clear_frames_grid()


                if (i == 120):
                    """show the kokarnas kamp"""
                    self.player.play("test.mp4")
                    self.clear_frames_grid()

                if (i == 999):
                    """exit"""
                    pass


        except Queue.Empty:
            pass

        self.root.after(50, self.que_reader)

    def clear_frames_grid(self):
        self.val_frame.grid_remove()
        self.host_label.grid_remove()
        self.kabaren_pic_frame.grid_remove()

    def show_intro_mov(self):
        """
        Handles the event of press on Instruction video button.
        Opens up Instruction video page.
        :return:
        """
        #self.master.bind('<Escape>', no_op)
        #self.back_button.config(command=no_op)

        #os.system('omxplayer "test.mp4"')

        #self.master.bind('<Escape>', self.back_button_handler)
        #self.back_button.config(command=self.back_button_handler)
        pass

    def show_bars_intro(self):
        pass

    def draw_kabaren_pic(self):
        temp_frame = Frame(self.frame, background = 'black')
        temp_frame.columnconfigure(0,weight=1)
        temp_frame.rowconfigure(0,weight=1)

        current_path = os.path.dirname(os.path.abspath(__file__))
        src_img_path = os.path.join(current_path, "img/kaba_pic.png")
        newsize = (1024, 600)
        img = Image.open(src_img_path)
        img.thumbnail(newsize)
        new_file_name = "kaba_pic" + ".png"
        img.save(new_file_name)
        img_path = os.path.join(current_path, new_file_name)
        self.kaba_img=PIL.ImageTk.PhotoImage(file=img_path)
        img_label = Label(master=temp_frame, image=self.kaba_img,background='black')
        #img_label = Label(temp_frame,text="hej")
        img_label.grid(row=0,column=0)
        return temp_frame

    def no_op(event):
        return 'break'