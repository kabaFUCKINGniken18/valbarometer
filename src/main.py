# -*- coding: utf-8 -*-

#!/usr/bin/python
import os, os.path
import argparse
import logging
from tkinterThread import TkApp
from server import StringGenerator
from server import StringGeneratorWebService
import cherrypy
import Queue

if __name__ == '__main__':
    # TK-inter walk through http://python-textbok.readthedocs.io/en/1.0/Introduction_to_GUI_Programming.html
    # print("start main")
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', "--fullscreen",
                        help="Turns on fullscreen display",
                        action="store_true")
    logger = logging.getLogger(__name__)
    args = parser.parse_args()

    fullscreen_on = False
    # Fullscreen selection
    if args.fullscreen:
        fullscreen_on = True


    # Tread safe que
    var_queue = Queue.Queue()


    # TkInter trhead
    # https://stackoverflow.com/questions/10556479/running-a-tkinter-form-in-a-separate-thread
    tk_tread = TkApp(fullscreen_on=fullscreen_on, var_queue=var_queue)
    tk_tread.start()


    # Http server
    # https://cherrypy.org/
    conf = {
        '/': {
            'tools.sessions.on': True,
            'tools.staticdir.root': os.path.abspath(os.getcwd())
        },
        '/generator': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.response_headers.on': True,
            'tools.response_headers.headers': [('Content-Type', 'text/plain')],
        },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './public'
        }
    }
    webapp = StringGenerator()
    webapp.generator = StringGeneratorWebService(var_queue)
    cherrypy.config.update({'server.socket_host': '0.0.0.0'})
    cherrypy.config.update({'server.socket_port': 8080})
    cherrypy.quickstart(webapp, '/', conf)
    tk_tread.stop_mainloop()
