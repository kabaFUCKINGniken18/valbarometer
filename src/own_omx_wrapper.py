
import os
from time import sleep
from omxplayer.player import OMXPlayer

# http://python-omxplayer-wrapper.readthedocs.io/en/latest/omxplayer/#module-omxplayer.player

class OwnOmxWrapper(object):
    def __init__(self, options):
        self.options = options
        self.omx_player = None



    def stop(self):
        if self.get_omx_status() is not None:
            self.omx_player.stop()
            self.omx_player = None



    def play(self, filename):
        if self.get_omx_status() is not None:
            self.stop()
            sleep(2)

        self.omx_player = OMXPlayer(filename,args=self.options)


    def pause(self):
        if self.omx_player is not None:
            if self.get_omx_status() is "Stopped":
                self.stop()

            else:
                self.omx_player.play_pause()


    def get_omx_status(self):
        if self.omx_player is not None:
            try:
                return self.omx_player.playback_status()
            except Exception as e:
                print e
                return None

        else:
            return None



