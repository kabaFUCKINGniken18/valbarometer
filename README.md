# README

Valbarometern

![progress-pic](Screenshot.png)
![progress-pic2](Screenshot2.png)

Webserver? https://docs.cherrypy.org/en/latest/tutorials.html

## virtual env setup

http://flask.pocoo.org/docs/0.12/installation/#installation

$ virtualenv venv
New python executable in venv/bin/python
Installing setuptools, pip............done.

Now, whenever you want to work on a project, you only have to activate the corresponding environment. On OS X and Linux, do the following:

$ . venv/bin/activate

Either way, you should now be using your virtualenv (notice how the prompt of your shell has changed to show the active environment).

And if you want to go back to the real world, use the following command:

$ deactivate

After doing this, the prompt of your shell should be as familiar as before.


## Depandancis

tkinter
```
pip install python-tk
```

CherryPy 
```
pip install CherryPy
```


pillow
```
sudo apt-get install libjpeg-dev zlib1g-dev
pip install Pillow --upgrade
```


omxplayer wraper
```
sudo apt-get update && sudo apt-get install -y libdbus-1-dev
pip install omxplayer-wrapper
```