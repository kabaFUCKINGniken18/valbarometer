raspberry_conf.md

| Key | Value |
| --- | --- |
| __Author__ | viktor.nybom@gmail.com | 
| __Updated__ | 2018-05-08 |

Download __raspbian-stretch-lite__ 
Make SD-card image with a suitable tool

run 
```
sudo raspi-config
```

start ssh
set local


## Set hostname for system

    sudo hostname kabaren-pi1

Edit `/etc/hostname`

        

and make sure that there is a line with

    kabaren-pi1

where YYMMDD is the date of SW setup.      

Edit `/etc/hosts`

```bash
sudo nano /etc/hosts
```

and make sure that there is a line with

    127.0.0.1     kabaren-pi1

where YYMMDD is the date of SW setup.      

Check with
    
    sudo hostnamectl


## Make sure that analog out is set in raspberry with volume.

```        
sudo amixer cset numid=3 1
sudo amixer sset PCM,0 80%
```

to get cleaner sound In /boot/config.txt add the following line:
```
audio_pwm_mode=2
```

## Change password

new password: kabakabakaffe
```
sudo chpasswd
kabakabakaffe
```


## Configuration of autostart of application

Create file in `~/` named `start.sh`
```sh
#!/bin/bash
python main.py -f
lxterminal -t "title" -e read -p "Press enter to continue" 
```

Make the file runable:

    sudo chmod +x start.sh


create `/usr/share/xsessions/kabaren.desktop`to include

```
Comment=kabaren
Exec=/home/pi/start.sh
# Icon=
Type=Application
```


## Configure startup

Edit `/etc/lightdm/lightdm.conf` and add/modify a section as follows

```
[Seat:*]
autologin-user=pi
xserver-command=X -s 0 -dpms
```

## Boot message handling 


Edit `/boot/cmdline.txt`so that it says

    dwc_otg.lpm_enable=0 root=/dev/mmcblk0p2 rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait quiet splash logo.nologo
    

Edit `/boot/config.txt` and add a line, at the end, with
    
    disable_splash=1

This removes a rainbow colored splash screen during boot


## Set console language

Edit `/etc/default/locale`and make sure we have the line

    LANG=en_GB.UTF-8

Edit `/etc/default/keyboard`and make sure we have the line

    XKBLAYOUT="us"



## omxplayer 

https://github.com/willprice/python-omxplayer-wrapper

omxplayer-wrapper

omxplayer-wrapper is a project to control OMXPlayer from python over dbus.
Installation:

You’ll need the following dependencies:

    libdbus-1
    libdbus-1-dev

OS pre-requisite installation

$ sudo apt-get update && sudo apt-get install -y libdbus-1{,-dev}

With pipenv

$ pipenv install omxplayer-wrapper

With Pip

$ pip install omxplayer-wrapper

Examples:
Playing local video file

#!/usr/bin/env python3

from omxplayer.player import OMXPlayer
from pathlib import Path
from time import sleep

VIDEO_PATH = Path("../tests/media/test_media_1.mp4")

player = OMXPlayer(VIDEO_PATH)

sleep(5)

player.quit()

Playing RTSP stream

#!/usr/bin/env python3

from omxplayer.player import OMXPlayer
from time import sleep

STREAM_URI = 'rtsp://184.72.239.149/vod/mp4:BigBuckBunny_175k.mov'

player = OMXPlayer(STREAM_URI)

sleep(8)

player.quit()

